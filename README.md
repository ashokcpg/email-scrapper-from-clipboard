**Please Run**
   
* git clone https://gitlab.com/ashokcpg/email-scrapper-from-clipboard   
* cd email-scrapper-from-clipboard
* pip install -r requirements.txt  

**Before running the script.**

**COPY** the large chunk of text containing emails, and then **RUN** the program.


> The emails scrapped will be copied to the clipboard, which you can paste in any Text Editor. 