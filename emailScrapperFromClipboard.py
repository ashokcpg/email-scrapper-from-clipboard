#! python3

import re, pyperclip

#Todo: Create a regex for email addresses
emailRegex = re.compile(r'''
    #some.+_thing@something.com
    [a-zA-Z0-9_.+]+
    @
    [a-zA-Z0-9_.+]+
''', re.VERBOSE)
#Todo Get the text off the clipboard
copiedText = pyperclip.paste()

#Todo: Checkpoint for empty clipboard
if copiedText == '':
    print("YOu need to copy the raw string and run this program.")
    finalResult=''
else:
    #Todo: Extract the email from the text
    extractedEmail = emailRegex.findall(copiedText)
    finalResult = '\n'.join(extractedEmail)
    #Todo: Copy the extracted email to the clipboard
    pyperclip.copy(finalResult)
#Todo: Shows message if no emails are found.
if finalResult == '':
    print('''There were no emails found on the text you had copied.\n Please try copying text that contains email.''')
else:
    numberOfEmails = len(finalResult.split('\n'))
    print(f'''***{numberOfEmails} email addresses were copied to your Clipboard.*** \n 
***Press Ctrl + V in any text editor.*** ''')



